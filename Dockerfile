FROM node:6.14.2
EXPOSE 8081
COPY server.js .
COPY index.html .
CMD node server.js
