var http = require('http');
var fs = require('fs')

var handleRequest = function(request, response) {
	response.writeHead(200, {'Content-type':'text/html'});
	fs.readFile('./index.html',null,function(error,data){
		if(error){
			response.writeHead(404);
			response.writeHead('File not found!')
		}else{
			response.write(data);
		}
		response.end();
	});
  	console.log('Received request for URL: ' + request.url);
};

var www = http.createServer(handleRequest);
www.listen(8081);